public class Encoding {
    public static void main(String args[]) {
        String s = "AAAABBBCCDAA";
        runLen(s);
    }
    public static void runLen(String s) {

        for (int i = 0; i <= s.length()-2; i++) {
            int count = 1;
            while (i < s.length() - 1 && s.charAt(i) == s.charAt(i + 1)) {
                count++;
                i++;
            }
            System.out.print(count);
            System.out.print(s.charAt(i));
        }
    }
}
