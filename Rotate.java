public class Rotate 
{
    public static void main(String args[])
    {
        Node root=null;
        Rotate a =new Rotate();
        root=a.insert(7,root);
        root=a.insert(7,root);
        root=a.insert(3,root);
        root=a.insert(5,root);
        a.print(root);
        System.out.println();
        root=a.rotateRight(2,root);
        a.print(root);
        System.out.println();
    }
    public Node rotateRight(int k,Node node)
    {
        if(node==null ||k<0)
        {
            return node;
        }
        int sizeOfList=getSizeOfList(node);
        k=k%sizeOfList;
        if(k==0)
        {
            return node;
        }
        Node temp=node;
        int i=1;
        while(i<sizeOfList-k)
        {
            temp=temp.next;
            i++;
        }
        Node t=temp.next;
        Node head=t;
        temp.next=null;
        while(t.next!=null)
        {
            t=t.next;
        }
        t.next=node;
        return head;

    }
    public int getSizeOfList(Node node)
    {
        if(node==null)
        {
            return 0;
        }
        return 1+getSizeOfList(node.next);
    }
    public Node getNew(int k)
    {
        Node a=new Node();
        a.next=null;
        a.data=k;
        return a;
    }
    public Node insert(int k,Node node)
    {
        if(node==null)
            return getNew(k);
        else
            node.next=insert(k,node.next);
        return node;
    }
    public void print(Node node)
    {
        if(node==null)
        {
            return;
        }
        System.out.print(node.data+" ");
        print(node.next);
    }
}
class Node{
    Node next;
    int data;
}

