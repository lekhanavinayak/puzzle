public class Permutation {
    public static void main(String args[])
    {
        int[] a={1,2,3};
        printPer(a,0);
    }
    public static void printPer(int a[],int b)
    {
        if(b==a.length-1)
        {
            printArr(a);
            return;
        }
        for(int i=b;i<a.length;i++)
        {
            swap(a,i,b);
            printPer(a,b+1);
            swap(a,i,b);
        }
    }
    public static void printArr(int a[])
    {
        for(int i=0;i<a.length;i++)
        {
            System.out.print(a[i] + " " );

        }
        System.out.println();
    }
    public static void swap(int a[],int i,int j)
    {
        int temp=a[i];
        a[i]=a[j];
        a[j]=temp;
    }
}
