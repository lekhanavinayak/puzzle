import java.util.HashMap;
import java.lang.*;
import java.io.*;

public class Dictionary {
    public static void main(String args[])throws IOException
    {
        HashMap<String,Object> a=new HashMap<>();
        a.put("key","3");
        HashMap<String,Object> b=new HashMap<>();
        HashMap<String,Object> output=new HashMap<>();
        b.put("baz","8");
        HashMap<String,Object> c=new HashMap<>();
        c.put("a","5");
        c.put("bar",b);
        a.put("foo",c);
        System.out.println(a);
        /*for(String k : a.keySet())
        {
            Object v=a.get(k);
            output.put(k,v);
            System.out.println(output);
        }*/
        flattenDict(a);
    }
    public static void flattenDict(HashMap<String,Object>  dict)
    {
        HashMap<Object,Object> output = new HashMap<>();
        for(Object k : dict.keySet())
        {
            Object v=(HashMap)dict.get(k);
            if(v instanceof String)
            {
                output.put(k, v);
            }
            else
            {
                flattenDict((HashMap)v);
            }
        }
    }
}
