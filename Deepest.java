public class Deepest {
    public static void main(String args[]) {
        Deepest b = new Deepest();
        DeepestNode root = b.createNewNode("a");
        root.left = b.createNewNode("b");
        root.right = b.createNewNode("c");
        root.left.left = b.createNewNode("d");
        b.deepestNode(root, 1, false);
        if (b.deepestNode != null) {
            System.out.println(b.deepestNode.data);
        } else {
            System.out.println("Deepest node");
        }
    }
    DeepestNode deepestNode;
    int current;
    public void deepestNode( DeepestNode node,int level,boolean ifLeft) {
        if (node == null) {
            return;
        }
        if (node.left == null && node.right == null && ifLeft && level > current) {
            deepestNode = node;
            current = level;
        }
        deepestNode(node.left, level + 1, true);
        deepestNode(node.right, level + 1, false);
    }
    public DeepestNode createNewNode(String val)
    {
        DeepestNode newNode = new DeepestNode();
        newNode.data=val;
        newNode.left=null;
        newNode.right=null;
        return newNode;
    }
}
class DeepestNode{
    DeepestNode left;
    DeepestNode right;
    String data;
}