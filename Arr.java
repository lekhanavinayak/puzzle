import java.util.HashSet;
public class Arr {
    public static void main(String args[]) {
        int arr[] = {5, 1, 3, 5, 2, 3, 4, 1};
        System.out.println(subArr(arr));
    }
    public static int subArr(int[] arr)
    {
        int i=0,j=1,max=0,current=1;
        max=Math.max(max,current);
        HashSet<Integer> set=new HashSet<Integer>();
        set.add(arr[0]);
        while(i<arr.length-1 && j<arr.length)
        {
            if(!set.contains(arr[j]))
            {
                current++;
                set.add(arr[j+1]);
            }
            else
            {
                set.remove(arr[i++]);
                current--;
            }
        }
        return Math.max(current,max);
    }
}

