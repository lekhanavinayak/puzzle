import java.util.HashMap;
import java.util.Map;

public class Palindrome 
{
    public static void main(String args[])
    {
        System.out.println(isPal("carrace"));
        System.out.println(isPal("daily"));
    }
    public static boolean isPal(String s)
    {
        Map<Character,Integer> a = new HashMap<>();
        for(Character c : s.toCharArray())
        {
            if(a.get(c)==null)
            {
                a.put(c,1);
            }
            else
            {
                a.put(c,a.get(c)+1);
            }
        }
        int oddChar=0;
        for(Integer val: a.values())
        {
            if(val%2!=0)
            {
                oddChar=oddChar+1;
            }
        }
        if(oddChar>1)
        {
            return false;
        }
        return true;
    }

}
