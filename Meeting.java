import java.util.Arrays;

public class Meeting {
    public static int meetingRoom(long[] start, long[]end) {
        Arrays.sort(start);
        Arrays.sort(end);
        int i=1;
        int j=0;
        int onGoing=1;
        int minMeeting=1;

        while(i<start.length && j<start.length)
        {
            if(start[i]<end[j]) {
                onGoing++;
                if (minMeeting < onGoing) {
                    minMeeting = onGoing;
                }
                i++;
            }

                else
                {
                    onGoing--;
                    j++;
                }
            }
        return minMeeting;

        }
        public static void main(String[] args){
            long start[]={30,0,60};
            long end[]={75,50,150};
            System.out.println(meetingRoom(start,end));
    }

}

