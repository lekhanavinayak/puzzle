public class Stack {
    int size = 1000;
    int top;
    int a[] = new int[size];

    boolean push(int x) {
        if (top >= (size - 1)) {
            System.out.println("Stack is full");
            return false;
        } else {
            a[++top] = x;
            System.out.println("Element pushed to Stack:" + x);
            return true;
        }

    }

    int pop() {
        if (top < 0) {
            System.out.println("Stack underflow");
            return 0;
        } else {
            int x = a[top--];
            return x;
        }
    }

    int max = a[0];
    int maximum()
    {
        for (int i = 0;i<a.length;i++)

        {
            if (a[i] > max)
                max = a[i];
        }
        return max;
    }

    public static void main(String args[]) {
        Stack stack=new Stack();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        System.out.println("Element poped from Stack:" + stack.pop() );
        System.out.println("Maximum element present in the Stack:" + stack.maximum());
    }
}