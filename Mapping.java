import java.util.HashMap;
import java.util.Map;

public class Mapping {
    public static void main(String args[])
    {
        Mapping m =new Mapping();
        System.out.println(m.map("abc","bcd"));
    }
    public boolean map(String s1,String s2) {
        if (s1.length() != s2.length()) {
            return false;
        }
        Map<Character, Character> a = new HashMap<>();
        Map<Character, Character> b = new HashMap<>();
        for (int i = 0; i < s1.length(); i++) {
            char c1 = s1.charAt(i);
            char c2 = s2.charAt(i);
            if (a.containsKey(c1)) {
                if (b.get(c1) != c2) {
                    return false;
                }
            }
                if (b.containsKey(c2)) {
                    if (b.get(c2) != c1) {
                        return false;
                    }
                }
                a.put(c1, c2);
                b.put(c2, c1);
            }
            return true;
        }
        }

