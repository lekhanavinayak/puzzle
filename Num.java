import java.util.Scanner;

public class Num {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter the n value");
        int n = sc.nextInt();
        System.out.println(n + "th Perfect Number:4" + getTen(n));
    }

    public static int getTen(int n) {
        int c = 0;
        for (int i = 1; ; i++) {
            int sum = 0;
            int x = i;
            while (x > 0) {
                sum = sum + x % 10;
                if (sum == 10)
                    c++;
                if (c == n)
                    return i;
                x = x / 10;
            }
        }
    }
}


