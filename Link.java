import java.util.HashSet;
import java.util.Set;
public class Link {
    int data;
    Link next;
}
class main
{
    public static Link push(Link head,int data)
    {
        Link link=new Link();
        link.data=data;
        link.next=head;
        return link;
    }
    public static Link findInt(Link first,Link second)
    {
        HashSet links=new HashSet();
        while(first!=null)
        {
            links.add(first.data);
            first=first.next;
        }
        while(second !=null)
        {
            if(links.contains(second.data)==true)
            {
                return second;
            }
            second=second.next;
        }
        return null;
    }
    public static void main(String args[])
    {
        Link first=null;
        for(int i=8;i>=1;i--)
        {
            first = push(first,i);
        }
        Link second =null;
        for(int i=14;i>=8;i--)
        {
            second=push(second,i);
        }
        //second.next.next.next=first.next.next.next;
        Link add=findInt(first,second);
        if(add!=null)
        {
            System.out.println("the intersection point is" + " " +add.data);
        }
        else
        {
            System.out.println("the list do not intersect");
        }
    }
}
