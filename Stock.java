public class Stock {
    public static void main(String args[]) {
        int[] a = {9, 11, 8, 5, 7, 10};
        int minMum = a[0];
        int p = 0;
        for (int i = 1; i < a.length; i++) {
            minMum = Math.min(minMum, a[i]);
            p = Math.max(p, a[i] - minMum);
        }
        System.out.println(p);
    }
}
