 class Link {

    public static class Linked{

    public int Value;{
            Object get;

            Object set;
            }

    public Linked Next;

    {
        Object get;

        Object set;

    }

    public Linked(int val) {
        Value = val;
    }

}
    public static void main(String args[]) {
        Linked n1 = new Linked(1);
        Linked n2 = new Linked(2);
        Linked n3 = new Linked(3);
        Linked n4 = new Linked(4);
        Linked n5 = new Linked(5);

        n1.Next = n2;
        n2.Next = n3;
        n3.Next = n4;
        n4.Next = n5;
        n5.Next = null;

        Linked head = rev(n1);
        print(head);
    }

    public static Linked rev(Linked node)
    {
        if(node==null || node.Next== null)
        {
            return node;
        }
        Linked head=rev(node.Next);
        node.Next.Next=node;
        node.Next=null;

        return head;
    }
    private static void print(Linked head)
    {
        while (head!=null)
        {
            System.out.print(head.Value);
            head=head.Next;
        }
    }

}
