package com.puzzel;

import java.util.*;

public class Puzzel {
    public static void main(String args[]) {
        int[] list = {10, 3, 5, 7};
        System.out.println(checksum(list, 17));
    }

    public static boolean checksum(int[] l, int k) {
        for (int i = 0; i < l.length; i++) {
            for (int j = i + 1; j < l.length; j++) {
                if (l[i] + l[j] == k) {
                    return true;
                }
            }
        }
        return false;
    }
}
