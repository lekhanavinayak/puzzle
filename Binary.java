public class Binary {
    public static void main(String args[])
    {
        Binary b=new Binary();
        BinaryNode root= b.createNewNode(10);
        root.left= b.createNewNode(5);
        root.right= b.createNewNode(5);
        root.left.right= b.createNewNode(2);
        root.right.right= b.createNewNode(1);
        root.right.right.left= b.createNewNode(-1);
        b.minSumFromRootToLeft(root,0);
        System.out.println(b.minSum);

    }
    int minSum=Integer.MAX_VALUE;
    public void minSumFromRootToLeft(BinaryNode node,int sum)
    {
        if(node==null)
        {
            return;
        }
        if(node.left==null && node.right==null && sum + node.data<minSum)
        {
            minSum=sum+node.data;
            return;
        }
        minSumFromRootToLeft(node.left,sum+node.data);
        minSumFromRootToLeft(node.right,sum+node.data);
    }
    public BinaryNode createNewNode(int val)
    {
        BinaryNode newNode=new BinaryNode();
        newNode.data=val;
        newNode.left=null;
        newNode.right=null;
        return newNode;
    }
}
class BinaryNode
{
    BinaryNode left;
    BinaryNode right;
    int data;
}