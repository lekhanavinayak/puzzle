package qustn_second;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;


@WebListener
public class Listener  implements HttpSessionAttributeListener
{
	
	public void attributeAdded(HttpSessionBindingEvent event)
	{
		System.out.println("added");
		System.out.println(event.getName()+":"+event.getValue());
	}
	public void attributeRemoved(HttpSessionBindingEvent event)
	{
		System.out.println("removed");
		System.out.println(event.getName()+":"+event.getValue());
	
	}
	public void attributeReplaced(HttpSessionBindingEvent event)
	{
		System.out.println("replaced");
		System.out.println(event.getName()+":"+event.getValue());
	}
	
	
	
	
	

}
