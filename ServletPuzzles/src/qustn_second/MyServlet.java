package qustn_second;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/MyServlet")			
public class MyServlet extends HttpServlet
{
	public void doGet(HttpServletRequest req,HttpServletResponse res) throws ServletException,IOException{
		res.setContentType("puzzl/html");
		PrintWriter pw= res.getWriter();
		
		
		HttpSession ses=req.getSession();
		ses.setAttribute("lekhana", "123"); 
		ses.setAttribute("pragati", "567");		
		ses.removeAttribute("lekhana");		
	}
}
