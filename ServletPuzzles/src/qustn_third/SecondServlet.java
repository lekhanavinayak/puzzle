package qustn_third;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SecondServlet {
	public void doGet(HttpServletRequest req,HttpServletResponse res) throws ServletException,IOException{
	      PrintWriter pw=res.getWriter();
	      pw.print("SECOND SERVLET");
	      
	}
}
