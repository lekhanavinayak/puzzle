package qustn_third;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class FirstServlet {
	public void doGet(HttpServletRequest req,HttpServletResponse res) throws ServletException,IOException{
	    RequestDispatcher rd= req.getRequestDispatcher("SecondServlet");
	    rd.forward(req, res);

	}
}
