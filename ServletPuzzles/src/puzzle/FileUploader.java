package puzzle;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

//import com.oreilly.servlet.MultipartRequest;

@WebServlet("/FileUpload")	
@MultipartConfig
public class FileUploader extends HttpServlet {
	public void doPost(HttpServletRequest req,HttpServletResponse res) throws ServletException,IOException {
		
		PrintWriter pw= res.getWriter();
		Part part=req.getPart("file");
		String filename=Paths.get(part.getSubmittedFileName()).getFileName().toString();
		String path="C:\\Users\\lekhana.bhat\\workspace\\Puzzle\\WebContent\\"+ filename;
		String overwrite = req.getParameter("rewrite");
		File f =new File(path);
		if(overwrite!=null)
		{
			if(f.exists())
			{
				f.delete();
				for(Part p: req.getParts())
				{
					p.write("C:\\Users\\lekhana.bhat\\workspace\\Puzzle\\WebContent\\"+filename);
				}
				pw.print("The file is overwritten successfully");
				
			}
			else
			{
				for(Part p: req.getParts())
				{
					p.write("C:\\Users\\lekhana.bhat\\workspace\\Puzzle\\WebContent\\"+filename);
				}
					pw.print("The file is written successfully");
												
			}
		}
		else
		{
			if(f.exists())
			{
				f.delete();
				for(Part p: req.getParts())
				{
					p.write("C:\\Users\\lekhana.bhat\\workspace\\Puzzle\\WebContent\\"+filename);
				}
				pw.print("The file is overwritten successfully");
				
			}
			else
			{
				for(Part p: req.getParts())
				{
					p.write("C:\\Users\\lekhana.bhat\\workspace\\Puzzle\\WebContent\\"+filename);
				}
				pw.print("The file is written successfully");
												
			}
			
		}
	}
}
