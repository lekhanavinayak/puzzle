import java.io.FileReader;
import java.io.IOException;

public class File {
    public static void main(String args[]) throws IOException {
        File f = new File();
        System.out.println(f.read(14));
    }

    String read(int n) throws IOException {
        String s = " ";
        FileReader f = new FileReader("f.txt");
        int i = f.read();
        int c = 0;
        while (i != -1) {
            if (c >= n) {
                break;
            }
            s = s + (char) i;
            i = f.read();
            c++;
        }
        f.close();
        return s;
    }
}